package com.example.gateway;

public class Constants {

  // Global orders
  public final static int DECISION_FILTER_ORDER = -100;
  public final static int SESSION_FILTER_ORDER = -90;

}
