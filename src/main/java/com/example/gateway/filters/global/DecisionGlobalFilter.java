package com.example.gateway.filters.global;

import com.example.gateway.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@ConditionalOnProperty(value = "global.filters.decision.enabled", havingValue = "true")
public class DecisionGlobalFilter implements GlobalFilter, Ordered {

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
    log.info("decision global filter");
    return chain.filter(exchange);
  }

  @Override
  public int getOrder() {
    return Constants.DECISION_FILTER_ORDER;
  }
}